import React from 'react';
import { Button } from 'reactstrap';
import { connect } from 'react-redux';
import { logInAction, logOutAction } from '../redux/authentication/actions';
import { Redirect, useHistory } from 'react-router-dom';

const Login = (props) => {
    const { isLogin, logInFunc, logOutFunc } = props;
    const history = useHistory();
    console.log('props login page =>', props);

    const handleClick = () => {
        if (isLogin) {
            // e.preventDefault();
            logOutFunc();
            history.push('/');
            // window.location.href = '/';
            // return <Redirect to="/" />
        } else {
            logInFunc();
            history.push('/counter');
            // e.preventDefault();
            // return <Redirect to="/counter" />
            // window.location.href = '/counter';
        }
    }



    return (
        <>
            <Button onClick={handleClick} color="info">{isLogin ? 'Logout' : 'Login'}</Button>
            {/* {isLogin ? <Redirect to='/' /> : <Redirect to="/counter" />} */}
        </>
    );
};

const mapStateToProps = (state) => ({
    isLogin: state.auth.isAuthenticated
});

const mapDispatchToProps = (dispatch) => ({
    logInFunc: () => dispatch(logInAction),
    logOutFunc: () => dispatch(logOutAction)
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);